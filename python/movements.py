class Movements(object):
	def switch_lane(self, current_lane):
		if current_lane == 'Left':
			return 'Right'
		else:
			return 'Left'

	def get_directions(self, position, throttle):
		ind_dir = [0.8, 0.7, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 1.0, 1.0, 1.0, 0.7, 0.7, 0.6, 0.5, 0.5, 0.5, 0.6, 'Right', 0.6, 0.6, 0.6, 0.6, 0.8, 0.8, 0.8, 0.6, 0.6, 0.8, 0.8, 0.6, 0.6, 0.6, 0.6, 'Left', 1.0, 1.0, 1.0, 1.0, 1.0]
		if ind_dir[int(position['piecePosition']['pieceIndex'])] == 'Left':
			return {"switch_lane" : self.switch_lane("Right"), "current_throttle" : throttle}
		elif ind_dir[int(position['piecePosition']['pieceIndex'])] == 'Right':
			return {"switch_lane" : self.switch_lane("Left"), "current_throttle" : throttle}
		else:
			return {"current_throttle" : ind_dir[int(position['piecePosition']['pieceIndex'])]}

